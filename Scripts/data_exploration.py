import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DATA_DIR = os.path.join(BASE_DIR, 'Data')
IMAGE_DIR = os.path.join(BASE_DIR, 'Images')

## Set matplotlb parameters:
plt.rcParams.update({
    'xtick.labelsize': 16,
    'ytick.labelsize': 16,
    'font.size': 15,
    'figure.autolayout': True,
    'figure.figsize': (7.2,4.45),
    'axes.titlesize' : 20,
    'axes.labelsize' : 17,
    'lines.linewidth' : 2,
    'lines.markersize' : 6,
    'legend.fontsize' : 13,
})

# plt.rc('lines', lw=3, c='r')

def total_time(full_df):
    ## Calculate the timeframe in which this data was collected.

    ## Print the values where the 'Time' is the lowest
    smallest_time = full_df['Time'].min()
    print('First observations')
    print(full_df[full_df['Time'] == smallest_time][['Time', 'Amount', 'Class']])

    ## Print the values where 'Time' is the largest
    largest_time = full_df['Time'].max()
    print('last observations')
    print(full_df[full_df['Time'] == largest_time][['Time', 'Amount', 'Class']])

    ## Find and print the total timeframe of the data in a markdown table format
    delta_seconds = largest_time - smallest_time
    delta_minutes = delta_seconds/60.0
    delta_hours   = delta_minutes/60.0
    delta_days    = delta_hours/24.0
    delta_months  = delta_hours/30.0

    output = """
            |    | timeframe|
            |:----|----------|
            | seconds | {0:.0f} |
            | minutes | {1:.2f} |
            | hours   | {2:.2f} |
            | days    | {3:.2f} |
             """

    print(output.format(delta_seconds,
                        delta_minutes,
                        delta_hours,
                        delta_days))

    pass


def statistics(full_df):
    ## Split the data into the two classes
    Fraud_df = full_df[full_df['Class'] == 1]
    Auth_df =  full_df[full_df['Class'] == 0]

    ## Create column of the log transform of the amount
    Fraud_df['log_Amount'] = Fraud_df['Amount'].apply(lambda a: np.log(a) if a!=0 else np.nan)
    Auth_df['log_Amount'] = Auth_df['Amount'].apply(lambda a: np.log(a) if a!=0 else np.nan)

    ## Print the averages of the amounts
    print("\nFradulent 'Amount' statistics:")
    print(Fraud_df['Amount'].describe())
    print("\nAuthentic 'Amount' statistics:")
    print(Auth_df['Amount'].describe())

    fig, ((ax3, ax4)) = plt.subplots(nrows=1, ncols=2, figsize=(24,12))

    ## Row 1
    # sns.distplot(a=Fraud_df['Amount'],bins=150, rug=True, ax=ax1)
    # sns.distplot(a=Auth_df['Amount'], bins=250, rug=True, ax=ax2, rug_kws={'height':0.02})

    # ## Row 2
    sns.distplot(a=Fraud_df['log_Amount'],bins=100, rug=True, ax=ax3)
    sns.distplot(a=Auth_df['log_Amount'], bins=100, rug=True, ax=ax4)

    # ax1.set_title("Fradulent Amount Distribution")
    # ax2.set_title("Authentic Amount Distribution")
    ax3.set_title("Fradulent Log Amount Distribution")
    ax4.set_title("Authentic Log Amount Distribution")

    # ax1.set_xlabel("Amount ($)")
    # ax2.set_xlabel("Amount ($)")
    ax3.set_xlabel("Log Amount")
    ax4.set_xlabel("Log Amount")

    fig.tight_layout()

    plt.savefig(os.path.join(IMAGE_DIR, 'log_class_histograms.png'), dpi=500)
    plt.show()
    plt.close()



def logistic_plots(full_df):

    # feat_cols = ['V1', 'V2', 'V3']

    feat_cols = full_df.drop(['Time', 'Class'], axis=1).columns.to_list()

    num_feats = len(feat_cols)
    ncols = 3
    nrows = int(np.ceil(num_feats/ncols))
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(24,60))

    for num, col in enumerate(feat_cols):
        r = int(num/ncols)
        c = num % ncols
        print("num ", num)
        # print("c ", c)
        # print("r ", r)
        sns.regplot(data=full_df, x=col, y='Class', ax=axes[r,c], logistic=True)

    # sns.scatterplot(x='V9', y='Class', data=full_df)
    # sns.scatterplot(x='Time', y='V1', data=full_df, hue='Class')

    plt.savefig(os.path.join(IMAGE_DIR, 'logistic_reg_plots.png'), dpi=500)
    plt.show()

def temporal_plots(full_df):

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(24,12))

    sns.scatterplot(x='Time', y='Amount', data=full_df, hue='Class')
    # sns.scatterplot(x='Time', y='V1', data=full_df, hue='Class')

    plt.show()

def boxplots(full_df):
    fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(12,12))
    sns.boxplot(data=full_df, x='Class', y='Amount', ax=ax)
    plt.show()
    pass

def main():
    ## Load the data
    full_df = pd.read_csv(os.path.join(DATA_DIR, 'creditcard.csv'))

    print(full_df)
    # temporal_plots(full_df)
    # logistic_plots(full_df)
    # boxplots(full_df)
    # statistics(full_df)
    # total_time(full_df)
    # missing_data(full_df)
if __name__ == "__main__":
    main()
