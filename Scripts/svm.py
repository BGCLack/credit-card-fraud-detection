import pandas as pd
import numpy as np
import os

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split, StratifiedKFold, cross_validate, GridSearchCV
from sklearn.preprocessing import StandardScaler


from sklearn.metrics import precision_recall_curve, average_precision_score, make_scorer

## Set matplotlb parameters:
plt.rcParams.update({
    'xtick.labelsize': 16,
    'ytick.labelsize': 16,
    'font.size': 15,
    'figure.autolayout': True,
    'figure.figsize': (7.2,4.45),
    'axes.titlesize' : 20,
    'axes.labelsize' : 17,
    'lines.linewidth' : 2,
    'lines.markersize' : 6,
    'legend.fontsize' : 13,
})

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DATA_DIR = os.path.join(BASE_DIR, 'Data')
IMAGE_DIR = os.path.join(BASE_DIR, 'Images')

def plot_score_vs_param(model,X, y, para, param_values):
    ## Set baseline score
    best_score = 0

    ## Make crossvalidator scorer with average precision score
    aps = make_scorer(average_precision_score)

    ## Set empty list for storing training and test scores
    cv_score_test = []
    cv_score_train = []
    values = param_values
    # C_values = [np.power(10.0,x) for x in np.arange(0,7,1)]
    for val in values:
        print(para + '_value: ' + str(val))
        if para == 'C':
            model.set_params(C=val)
        if para == 'gamma':
            model.set_params(gamma=val)
        if para == 'degree':
            model.set_params(degree=val)
        scores = cross_validate(model, X, y, return_train_score=True, n_jobs=-1, scoring=aps)
        print('test score: ', scores['test_score'].mean())
        print('train score: ', scores['train_score'].mean())
        cv_score_test.append(scores['test_score'].mean())
        cv_score_train.append(scores['train_score'].mean())

        ## If the iteraction is the current best perfomer, save the parameters and model
        if scores['test_score'].mean() > best_score:
            best_score = scores['test_score'].mean()
            model.fit(X,y)
            best_model = model
            best_value = val

    print('Best value of cost (C): ', best_value)
    print('Best test set score: ', round(best_score, 3))

    results = {}
    results['best_test_score'] = best_score
    results['best_model'] = best_model
    results['best_value'] = best_value
    results['train_scores'] = cv_score_train
    results['test_scores'] = cv_score_test

    return results

def preprocessing(full_df):
    ## drop the transactions with 'Amount' == 0
    proc_df = full_df.drop(full_df[full_df['Amount'] == 0].index)

    scaler = StandardScaler()
    proc_df[['Amount']] = scaler.fit_transform(proc_df[['Amount']])

    for col in proc_df.columns:
        print("{}: mean = {:.2f}, std = {:2f}".format(col, proc_df[col].mean(), proc_df[col].std()))

    return proc_df

def loop_poly_svm(proc_df):
    """
    Runs a simple crossvalidated radial SVC model in a for loop to test different hyper parameters.
    """
    X = proc_df.drop(['Class', 'Time'], axis=1)
    # X = proc_df[['Amount', 'V1', 'V2']]
    y = proc_df['Class']

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=101, train_size=5000)

    orginal_class_split = proc_df['Class'].value_counts()[0]/len(proc_df)
    training_class_split = y_train.value_counts()[0]/len(y_train)
    test_class_split = y_test.value_counts()[0]/len(y_test)

    ## Instantiate the svc with polynomial kernel
    svm = SVC(kernel='poly', C=0.1, max_iter=10000000)
    ## Create the list of polynomial degress to iterate over
    degree_values = [1,2,3,4,5,6]
    ## Define grid of gamma values and C values
    gamma_values = [np.power(10.0,x) for x in np.arange(-9,7,1)]
    C_values = [np.power(10.0,x) for x in np.arange(-7,14,1)]

    ## Set up figure to plot train, test scores
    fig, axs = plt.subplots(nrows=len(degree_values), ncols=2, figsize=(24,60))
    plt.subplots_adjust(hspace=0.5)
    ## For each degree produce a new row in the plot
    for idx, deg in enumerate(degree_values):
        print("Degree: ", deg)
        ## Update model with new polynomial degree
        svm.set_params(degree=deg)
        ## Perform CV with gamma values
        gamma_results = plot_score_vs_param(svm, X_train, y_train, para='gamma', param_values=gamma_values)
        ## Update model with best gamma value
        svm.set_params(gamma=gamma_results['best_value'])
        ## Perform CV with C values
        C_results = plot_score_vs_param(svm, X_train, y_train, para='C', param_values=C_values)

        ## Update axes with new plots
        axs[idx,0].plot(gamma_values, gamma_results['test_scores'], label='test score')
        axs[idx,0].plot(gamma_values, gamma_results['train_scores'], label='train_score')
        axs[idx,0].set_xlabel('gamma')
        axs[idx,0].set_ylabel('Score')
        axs[idx,0].set_title('Model performance for gamma (Degree {})'.format(deg))
        axs[idx,0].legend(fontsize = 'large')
        axs[idx,0].set_xscale('log')
        score_label = "Best Test Score: {:.2f}".format(gamma_results['best_test_score'])
        axs[idx,0].text(0.1, 0.8, ha='center', va='center', s=score_label , fontsize=12, transform=axs[idx,0].transAxes)

        axs[idx,1].plot(C_values, C_results['test_scores'], label='test score')
        axs[idx,1].plot(C_values, C_results['train_scores'], label='train_score')
        axs[idx,1].set_xlabel('C')
        axs[idx,1].set_ylabel('Score')
        axs[idx,1].set_title('Model performance for C (Degree: {0}, gamma: {1:.2})'.format(deg,gamma_results['best_value']))
        score_label = "Best Test Score: {:.2f}".format(C_results['best_test_score'])
        axs[idx,1].legend(fontsize = 'large')
        axs[idx,1].text(0.1, 0.8, ha='center', va='center', s=score_label , fontsize=12, transform=axs[idx,1].transAxes)
        axs[idx,1].set_xscale('log')


    # fig.tight_layout()
    plt.savefig(os.path.join(IMAGE_DIR, 'poly_kernel_cv_plots.png'), dpi = 200)
    plt.show()
    plt.close()

def loop_rbf_svm(proc_df):
    """
    Runs a simple crossvalidated radial SVC model in a for loop to test different hyper parameters.
    """


    X = proc_df.drop(['Class', 'Time'], axis=1)
    # X = proc_df[['Amount', 'V1', 'V2']]
    y = proc_df['Class']

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=101, train_size=5000)

    orginal_class_split = proc_df['Class'].value_counts()[0]/len(proc_df)
    training_class_split = y_train.value_counts()[0]/len(y_train)
    test_class_split = y_test.value_counts()[0]/len(y_test)

    svm = SVC(kernel='rbf', C=1, max_iter=10000000)
    gamma_values = [np.power(10.0,x) for x in np.arange(-7,7,1)]
    gamma_results = plot_score_vs_param(svm, X_train, y_train, para='gamma', param_values=gamma_values)
    svm.set_params(gamma=gamma_results['best_value'])
    C_values = [np.power(10.0,x) for x in np.arange(-7,7,1)]
    C_results = plot_score_vs_param(svm, X_train, y_train, para='C', param_values=C_values)


    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(12,6))
    ## Update axes with new plots
    ax1.plot(gamma_values, gamma_results['test_scores'], label='test score')
    ax1.plot(gamma_values, gamma_results['train_scores'], label='train_score')
    ax1.set_xlabel('gamma')
    ax1.set_ylabel('Score')
    ax1.set_title('Model performance for gamma')
    ax1.legend(fontsize = 'large')
    score_label = "Best Test Score: {:.2f}".format(gamma_results['best_test_score'])
    ax1.text(0.1, 0.8, ha='center', va='center', s=score_label , fontsize=12, transform=ax1.transAxes)
    ax1.set_xscale('log')

    ax2.plot(C_values, C_results['test_scores'], label='test score')
    ax2.plot(C_values, C_results['train_scores'], label='train_score')
    ax2.set_xlabel('C')
    ax2.set_ylabel('Score')
    ax2.set_title('Model performance for C')
    ax2.legend(fontsize = 'large')
    score_label = "Best Test Score: {:.2f}".format(C_results['best_test_score'])
    ax2.text(0.1, 0.9, ha='center', va='center', s=score_label , fontsize=12, transform=ax2.transAxes)
    ax2.set_xscale('log')

    plt.tight_layout()
    plt.savefig(os.path.join(IMAGE_DIR, 'rbf_cv_plots.png'))
    plt.show()
    plt.close()

def looped_lin_svm(proc_df):
    """
    Runs a simple crossvalidated linear SVC model in a for loop to test different hyper parameters.
    """


    X = proc_df.drop(['Class', 'Time'], axis=1)
    # X = proc_df[['Amount', 'V1', 'V2']]
    y = proc_df['Class']

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=101, train_size=5000)

    orginal_class_split = proc_df['Class'].value_counts()[0]/len(proc_df)
    training_class_split = y_train.value_counts()[0]/len(y_train)
    test_class_split = y_test.value_counts()[0]/len(y_test)

    svm = SVC(kernel='linear', max_iter=10000000)
    C_values = [np.power(10.0,x) for x in np.arange(-7,7,1)]
    C_results = plot_score_vs_param(svm, X_train, y_train, para='C', param_values=C_values)

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(12,12))
    ax.plot(C_values, C_results['test_scores'], label='test score')
    ax.plot(C_values, C_results['train_scores'], label='train_score')
    ax.set_xlabel('C')
    ax.set_ylabel('Score')
    ax.set_title('Model performance for C')
    ax.legend(fontsize = 'large')
    ax.set_xscale('log')
    score_label = "Best Test Score: {:.2f}".format(C_results['best_test_score'])
    ax.text(0.1, 0.9, ha='center', va='center', s=score_label , fontsize=12, transform=ax.transAxes)
    plt.savefig(os.path.join(IMAGE_DIR, "linear_kernal_CV_plot.png"), dpi=500)
    plt.show()

def simple_svm(proc_df):
    """
    Runs a simple SVC model with no cross validation. Produces a precision precision recall curve.
    """


    X = proc_df.drop(['Class', 'Time'], axis=1)
    # X = proc_df[['Amount', 'V1', 'V2']]
    y = proc_df['Class']

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=101, train_size=5000)

    orginal_class_split = proc_df['Class'].value_counts()[0]/len(proc_df)
    training_class_split = y_train.value_counts()[0]/len(y_train)
    test_class_split = y_test.value_counts()[0]/len(y_test)


    print("Original split \t 0: {:.2f}%, 1: {:.2f}%".format(orginal_class_split*100, (1-orginal_class_split)*100))
    print("training set split \t 0: {:.2f}%, 1: {:.2f}%".format(training_class_split*100, (1-training_class_split)*100))
    print("test set split \t 0: {:.2f}%, 1: {:.2f}%".format(test_class_split*100, (1-test_class_split)*100))

    svm = SVC(kernel='linear')
    svm.fit(X_train, y_train)
    print("SVM fitted")
    y_preds = svm.predict(X_test)

    precision, recall, thresholds = precision_recall_curve(y_test, y_preds)

    ## Print averge precision score
    aps = average_precision_score(y_test, y_preds)
    print("Average Precision Score: ", aps)

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(12,12))
    sns.lineplot(x=precision, y=recall, ax=ax)
    plt.show()

def poly_svm(proc_df):

    X = proc_df.drop(['Class', 'Time'], axis=1)
    y = proc_df['Class']

    skf = StratifiedKFold(n_splits=5)

    svm = SVC(kernel='linear')

    aps = make_scorer(average_precision_score)

    cv_scores = cross_validate(svm, X, y, scoring=aps, return_train_score=True)

    cv_output = """
                Test Scores : {0}
                Average Test Score: {1:.2f}
                Train Scores : {2}
                Average Train Score : {3:.2f}
                Fit Time : {4}

                """

    print(cv_output.format(cv_scores['test_score'],
                           np.mean(cv_scores['test_score']),
                           cv_scores['train_score'],
                           np.mean(cv_scores['train_score']),
                           cv_scores['fit_time'],
                           ))

def grid_search(proc_df):
    """
    Grid search the hyperparameters
    """
    X = proc_df.drop(['Class', 'Time'], axis=1)
    # X = proc_df[['Amount', 'V1', 'V2']]
    y = proc_df['Class']

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=101, train_size=50000)

    param_grid = {
        'C': [np.power(10.0,x) for x in np.arange(-5,5,1)],
        'gamma': [np.power(10.0,x) for x in np.arange(-5,5,1)],
        # 'kernel':('poly'),
        # 'degree': [1,2,3],
    }

    svm = SVC(kernel='rbf', C=0.1, max_iter=1000000)
    aps = make_scorer(average_precision_score)
    gscv = GridSearchCV(svm, param_grid=param_grid, scoring=aps, n_jobs=-1, verbose=5)

    gscv.fit(X_train, y_train)
    # print(gscv.cv_results_)
    print("Best Score: ", gscv.best_score_)
    print("With Parameters: ", gscv.best_params_)


def main():
    ## Load the data
    full_df = pd.read_csv(os.path.join(DATA_DIR, 'creditcard.csv'))

    proc_df = preprocessing(full_df)

    X = proc_df.drop(['Class', 'Time'], axis=1)
    # X = proc_df[['Amount', 'V1', 'V2']]
    y = proc_df['Class']

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=101, train_size=5000, stratify=y)

    orginal_class_split = proc_df['Class'].value_counts()[0]/len(proc_df)
    training_class_split = y_train.value_counts()[0]/len(y_train)
    test_class_split = y_test.value_counts()[0]/len(y_test)

    print("Original split \t 0: {:.2f}%, 1: {:.2f}%".format(orginal_class_split*100, (1-orginal_class_split)*100))
    print("training set split \t 0: {:.2f}%, 1: {:.2f}%".format(training_class_split*100, (1-training_class_split)*100))
    print("test set split \t 0: {:.2f}%, 1: {:.2f}%".format(test_class_split*100, (1-test_class_split)*100))

    # poly_svm(proc_df)
    # simple_svm(proc_df)
    # looped_lin_svm(proc_df)
    # loop_rbf_svm(proc_df)
    # loop_poly_svm(proc_df)
    grid_search(proc_df)



if __name__ == "__main__":
    main()
